![projectbetamainpage.PNG](./projectbetamainpage.PNG)
# CarCar - Team
Randy & Vincent - Inventory

Randy Angel - Sales

Vincent Lee - Service

## How to Run this Application:
1. Fork and clone the project from the link https://gitlab.com/randyangel84/project-beta.git

2. Change Directory (CD) into cloned project. Please run following the following commands....

* docker volume create beta-data
* docker-compose build
* docker-compose up

3. Please wait for docker containers to fully function; should take a couple minutes...

4. When the docker containers are up and running navigate to http://localhost:3000/ to see the application.
   If localhost:3000 is not working; make migrations for sales & service apis.

5. Open up insomnia to access the RESTful APIs and use the ports and paths provided under each microservice below

!!!!!!!!!! IF CREATING SOMETHING NEW ON WEBSITE DOES NOT SHOW/WORK; REFRESH PAGE PLEASE !!!!!!!!!!

All Sales Records

Sales by Employee

All Manufacturers

All Technicians

Service History

Service Appointments


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## Services
localhost:3000 (React Front End)

localhost:15432 (Database)

localhost:8100 (Inventory API)

localhost:8080 (Service API)

localhost:8090 (Sales API)

## Application Diagram
![ProjectBetaDiagram.PNG](./pictures/ProjectBetaDiagram.PNG)


## API Documentation
## Inventory Microservice
![ProjectBetaDiagram-InventoryMicroservice-1.PNG](./pictures/ProjectBetaDiagram-InventoryMicroservice-1.PNG)
* ## Manufacturer:
    * _______________
         ## GET - http://localhost:8100/api/manufacturers/ ![readmeinventorymodel.PNG](./pictures/readmeinventorymodel.PNG)
         ## POST - http://localhost:8100/api/manufacturers/ ![readmeinventorymodel1.PNG](./pictures/readmeinventorymodel1.PNG)
         ## GET - http://localhost:8100/api/manufacturers/:id/ ![readmeinventorymodel2.PNG](./pictures/readmeinventorymodel2.PNG)
         ## PUT - http://localhost:8100/api/manufacturers/:id/<int:pk>/ ![readmeinventory3.PNG](./pictures/readmeinventory3.PNG)
         ## DELETE - http://localhost:8100/api/manufacturers/:id/<int:pk>/ ![readmeinventory4.PNG](./pictures/readmeinventory4.PNG)

* ## VehicleModel
    * _______________
        ## GET - http://localhost:8100/api/models/ ![readmeinventorymodel.PNG](./pictures/readmeinventorymodel.PNG)
        ## POST - http://localhost:8100/api/models/ ![readmeinventorymodel1.PNG](./pictures/readmeinventorymodel1.PNG)
        ## GET - http://localhost:8100/api/models/:id/ ![readmeinventorymodel2.PNG](./pictures/readmeinventorymodel2.PNG)
        ## PUT - http://localhost:8100/api/models/:id/ ![readmeinventorymodel3.PNG](./pictures/readmeinventorymodel3.PNG)
        ## DELETE - http://localhost:8100/api/models/:id/ ![readmeinventorymodel4.PNG](./pictures/readmeinventorymodel4.PNG)

* ## Automobile
    * _______________
        ## GET - http://localhost:8100/api/automobiles/ ![readmeauto.PNG](./pictures/readmeauto.PNG)
        ## POST - http://localhost:8100/api/automobiles/ ![readmeauto4.PNG](./pictures/readmeauto4.PNG)
        ## GET - http://localhost:8100/api/automobiles/:id/ ![readmeauto1.PNG](./pictures/readmeauto1.PNG)
        ## PUT - http://localhost:8100/api/automobiles/:vin/ ![readmeauto2.PNG](./pictures/readmeauto2.PNG)
        ## DELETE - http://localhost:8100/api/automobiles/:vin/ ![readmeauto3.PNG](./pictures/readmeauto3.PNG)

## Sales Microservice
![ProjectBetaDiagram-SalesMicroservice.PNG](./pictures/ProjectBetaDiagram-SalesMicroservice.PNG)
* ## AutomobileVO:
    * The model integrates with the inventory microservice by creating a value object with a unique vin property. This enables us to retrieve the individual values of automobile from the inventory microservice, enabling a foreign key relationship between the SalesRecord and Automobile in the inventory_rest. This is done by making a function that polls the inventory microservice for information on automobiles using the vin number.
    Getting DATA from INVENTORY MICROSERVICE

* ## Customer
    * The customer model allows us to create a customer with three properties: name, address, and phone number. This customer can then be related to a SalesRecord.
         ## GET - http://localhost:8090/api/customers/ ![readmecustomer1.PNG](./pictures/readmecustomer1.PNG)
         ## POST - http://localhost:8090/api/customers/ ![readmecustomer2.PNG](./pictures/readmecustomer2.PNG)
         ## DELETE - http://localhost:8090/api/customers/<int:pk>/ ![readmecustomer3.PNG](./pictures/readmecustomer3.PNG)

* ## SalesPerson
    * The SalesPerson model allows us to create a salesperson with three properties: name, employee number, and sales made. This salesperson can then be related to a SalesRecord.
        ## GET - http://localhost:8090/api/sales-persons/ ![readmesalesrep1.PNG](./pictures/readmesalesrep1.PNG)
        ## POST - http://localhost:8090/api/sales-persons/ ![readmesalesrep2.PNG](./pictures/readmesalesrep2.PNG)
        ## GET - http://localhost:8090/api/sales-persons/<int:pk>/ ![readmesalesrep-2.PNG](./pictures/readmesalesrep-2.PNG)
        ## DELETE - http://localhost:8090/api/sales-persons/<int:pk>/ ![readmesalesrep3.PNG](./pictures/readmesalesrep3.PNG)

* ## SalesRecord
    * The SalesRecord model allows us to create a sales record that uses the three other models (customer, salesperson, and automobile) as ForeignKey relationships. It also has an additional property of sales price
        ## GET - http://localhost:8090/api/sales-records/ ![readmesalesrecord-2.PNG](./pictures/readmesalesrecord-2.PNG)
        ## POST - http://localhost:8090/api/sales-records/ ![readmesalesrecord1.PNG](./pictures/readmesalesrecord1.PNG)


## Services Microservice
![ProjectBetaDiagram-ServiceMicroservice.PNG](./pictures/ProjectBetaDiagram-ServiceMicroservice.PNG)
* ## Technicians
    * This model allows us to create and list a technician by the employee's name and number.
        ## GET - http://localhost:8080/api/technicians/ ![readmetechnicianGET.PNG](./pictures/readmetechnicianGET.PNG)
        ## POST - http://localhost:8080/api/technicians/ ![readmetechnicianPOST.PNG](./pictures/readmetechnicianPOST.PNG)

* ## Services
    * This model allows us to create, list, and check the services as well as the service history and appointments.
        ## GET - http://localhost:8080/api/appointments/ ![readmeservicesGET.PNG](./pictures/readmeservicesGET.PNG)
        ## POST - http://localhost:8080/api/appointments/ ![readmeservicesPOST.PNG](./pictures/readmeservicesPOST.PNG)
        ## GET - http://localhost:8080/api/appointments/1/ ![readmeservicesGET2.PNG](./pictures/readmeservicesGET2.PNG)
