import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'


export default function VehicleInventory({ InventoryList }) {
    return (
        <>
            <h1 className="col-lg-4 bdr mx-auto bg-dark mt-5 p-4 text-center" style={{color: "white"}}>Vehicle Inventory</h1>
            <table className="table bdr table-hover table-info table-dark mt-1">
            <thead class="thead-dark">
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {InventoryList && InventoryList.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.vin}</td>
                                <td>{vehicle.color}</td>
                                <td>{vehicle.year}</td>
                                <td>{vehicle.model.name}</td>
                                <td>{vehicle.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/automobiles/new/" style={{color: "white"}}>Add a Automobile</NavLink>
      </button>
        </>
    )
}