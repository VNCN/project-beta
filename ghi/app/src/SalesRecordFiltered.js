import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'


export default function SalesRecordFiltered({ salesRecords }) {

    const [search, setSearch] = useState("");
    useEffect(() => { }, [])
    return (
        <>
            <h1 className="col bdr mx-auto bg-dark mt-5 p-2 text-center" style={{color: "white"}}>All Manufacturers
                Sales Filtered By Sales Person
            </h1>
            <div className="container" >
                <div className="pb row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group p-3 mb-2 bg-dark text-white">
                            <div className="input-group p-3 mb-2 bg-dark text-white" >
                                <input onChange={event => setSearch(event.target.value)} className="form-control" type="text " placeholder="Search by Sales Person (Case-Sensitive)" />
                            </div>
                        </div>
                    </form>
                </div>
                <table className="table bdr table-hover table-info table-dark">
            <thead class="thead-dark">
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Automobile VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesRecords && salesRecords
                            .filter(record => record.sales_person.name.includes(search))
                            .map(record => {
                                return (
                                    <tr key={record.id}>
                                        <td>{record.sales_person.name}</td>
                                        <td>{record.customer.name}</td>
                                        <td>{record.vin}</td>
                                        <td>{record.sales_price}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
            <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/customers/new/" style={{color: "white"}}>Add New Customer</NavLink>
      </button>
      <button type="button" className=" btn btn-dark btn-sm " >
        <NavLink className="fs-6" aria-current="page" to="/sales-persons/new/" style={{color: "white"}}>Hire New Sales Representative</NavLink>
      </button>
        </>
    )
}
