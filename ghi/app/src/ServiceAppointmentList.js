import React from "react";
import { NavLink } from 'react-router-dom'

function ServiceAppointmentList({ AppointmentList }) {
    console.log(AppointmentList)
    const deleteItem = async (event) => {
      const deleteTag = event.currentTarget.id;

      fetch(`http://localhost:8080/api/appointments/${deleteTag}`,{
        method: 'delete',
        headers: {'Content-Type': 'application/json'
      }
      })
    }
    const finishAppt = async (event) => {
      const finishTag = event.currentTarget.id;
      await fetch(`http://localhost:8080/api/appointments/${finishTag}/`,{
          method: 'put',
          body: JSON.stringify({finished: true}),
          headers: {'Content-Type': 'application/json'},
      })
    }
  const handleSubmit = (id) => {
    finishAppt(id);
  };

      return(
        <>
        <h1 className=" bdr mx-auto bg-dark mt-5 w-50 p-4 text-center" style={{color: "white"}}>Service Appointments</h1>
        <table className="table bdr table-hover table-info table-dark mt-2">
            <thead class="thead-dark">
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
            <th>Service Done?</th>
          </tr>
        </thead>
        <tbody>
          { AppointmentList && AppointmentList.map(appointment => {
            if (appointment.vip) {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name }</td>
                  <td>{ appointment.date }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.technician }</td>
                  <td>{ appointment.reason }</td>
                  <td>yes</td>
                    <td>
                      <button onClick={deleteItem} id={appointment.id} key={appointment.id}>Cancel</button>
                      <button onClick={handleSubmit} id={appointment.id} key={appointment.id}>Finished!</button>
                    </td>
                </tr>)
            } else {
              return(
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name }</td>
                  <td>{ appointment.date }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.technician }</td>
                  <td>{ appointment.reason }</td>
                  <td>no</td>
                    <td>
                      <button onClick={deleteItem} id={appointment.id} key={appointment.id}>Cancel</button>
                      <button onClick={handleSubmit} id={appointment.id} key={appointment.id}>Finished</button>
                    </td>
                </tr>)
            }
          })}
        </tbody>
      </table>
      <button className="btn btn-dark float-end">
       <NavLink className="fs-6" aria-current="page" to="/services/new" style={{color: "white"}}>Create Service Appointment</NavLink>
      </button>
      </>
      )
  }

  export default ServiceAppointmentList
