import { NavLink } from 'react-router-dom'

function ServiceTechList({ TechnicianList }) {
  return (
    <>
      <h1 className=" bdr mx-auto bg-dark mt-5 w-25 p-4 text-center" style={{color: "white"}}>Technician List</h1>
      <table className="table bdr table-hover table-info table-dark mt-2">
            <thead class="thead-dark">
          <tr>
            <th>Tech Name</th>
            <th>Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {TechnicianList && TechnicianList.map(tech => {
            return (
              <tr key={tech.id}>
                <td>{tech.name}</td>
                <td>{tech.employee_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/technicians/new" style={{color: "white"}}>Add a Technician</NavLink>
      </button>
    </>
  );
}

export default ServiceTechList